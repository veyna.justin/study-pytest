make_venv='false'

while getopts 'v' flag; do
  case "${flag}" in
    v) make_venv='true' ;;
  esac
done
echo $make_venv
if [ $make_venv == 'true' ]; then
  if [ -d "pytest_venv" ]; then
    echo "pytest_venv exists. Deleting."
    rm -rf pytest_venv
    source pytest_venv/bin/activate
  else
    echo "Making pytest_venv."
    python3 -m venv pytest_venv
    source pytest_venv/bin/activate
  fi
fi

pip install -r requirements.txt
pip install -e .

if [ $make_venv == 'true' ]; then
  deactivate
fi
