# Study Pytest

Experimenting around with PyTest, Mocker and while I'm at it, CI/CD.

## Resources
PyTest Documentation (LINK)[https://docs.pytest.org/en/stable/contents.html]
GitLab CI/CD Tutorial1 (LINK)[https://gitlab.com/help/ci/quick_start/README#creating-a-gitlab-ciyml-file]
GitLab CI/CD Tutorial2 (LINK)[https://medium.com/cubemail88/setting-gitlab-ci-cd-for-python-application-b59f1fb70efe]

### CI/CD
The built-in gitlab runner seems to use docker to run instances.

### pytest
#### Fixtures
Fixtures are like glorified factories with additional test-centric features
- Great for testing several instance of one class in the same way
- Poor for testing anything that requires several instances of the same class
  - unless you make a fixture that makes and returns several instances of the same class
- cant readily test code that happens inside of the fixture so that must be tested separately
### pytest-mock
#### mocker
I though this test would be sufficient to use a mocker but in this case the code is simple enough and there are no long-waits or environment-dependent io calls
- As a note i can see this be used to replace:
  - network writes/reads
  - creation of objects from large files
  - creation of unwanted files
