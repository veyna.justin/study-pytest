class ShallowData():
    def __init__(self, a="foo", b="bar"):
        self.a = a
        self.b = b

    def make_new_shallow_data(self):
        a = self.a + self.b
        b = self.b + self.a
        return ShallowData(a, b)

    def __eq__(self, other):
        return self.a == other.a and self.b == other.b
