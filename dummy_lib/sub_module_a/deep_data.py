from .shallow_data import ShallowData
class DeepData():
    def __init__(self):
        self.sda = ShallowData()
        self.sdb = self.sda.make_new_shallow_data()
    def make_new_shallow_data(self):
        a = self.sda.a + self.sdb.b
        b = self.sdb.a + self.sdb.a
        return ShallowData(a, b)
