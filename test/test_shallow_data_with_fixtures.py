import pytest

from dummy_lib import ShallowData

@pytest.fixture(scope="module",
                params=
                [
                    ["foo", "bar"],
                    ["test", "py"],
                ])
def shallow_data(request):
    return ShallowData(*request.param)

def test_shallow_data_init(shallow_data):
    """ unable to check that args are set properly if i use a fixture here
    """
def test_make_new_shallow_data(shallow_data):
    pattern_a = shallow_data
    pattern_b = shallow_data.make_new_shallow_data()
    pattern_c = pattern_b.make_new_shallow_data()

    aa = pattern_a.a
    ab = pattern_a.b
    ba = aa+ab
    bb = ab+aa
    ca = ba+bb
    cb = bb+ba
    assert pattern_b.a == ba and pattern_b.b == bb
    assert pattern_c.a == ca and pattern_c.b == cb
