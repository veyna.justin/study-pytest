import pytest

from dummy_lib import ShallowData

def test_shallow_data_init():
    pattern_a = ShallowData()
    pattern_b = ShallowData("foo","bar")
    pattern_c = ShallowData("test","py")
    pattern_d = ShallowData("test",b="py")

    assert pattern_a.a == "foo" and pattern_a.b == "bar"
    assert pattern_b.a == "foo" and pattern_b.b == "bar"
    assert pattern_c.a == "test" and pattern_c.b == "py"
    assert pattern_c.a == "test" and pattern_d.b == "py"

    assert pattern_a == pattern_b
    assert pattern_c == pattern_d

def test_make_new_shallow_data():
    pattern_a = ShallowData()
    pattern_b = pattern_a.make_new_shallow_data()
    pattern_c = pattern_b.make_new_shallow_data()

    assert pattern_b.a == "foobar" and pattern_b.b == "barfoo"
    assert pattern_c.a == "foobarbarfoo" and pattern_c.b == "barfoofoobar"
