if [ -d "pytest_venv" ]
then
source pytest_venv/bin/activate
fi

pytest

if [ -d "pytest_venv" ]
then
deactivate
fi
